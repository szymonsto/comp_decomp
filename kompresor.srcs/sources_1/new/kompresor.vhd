----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.01.2020 12:00:42
-- Design Name: 
-- Module Name: kompresor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--entity lzw is
  --  port(
        --Bit stream: in STD LOGIC:
        --Integer_stream: out STD_LOGICVECTOR (7 downto 0);
       -- Integer_stream_dec: in STD_LOGICVECTOR (7 downto 0);
        --Bit stream_de: in STD LOGIC:
       -- output_sel: out STD_LOGIC;
       -- clock: in STD_LOGIC;
       -- Selector: in STD_LOGIC_VECTOR(1 downto 0);                
   -- );
--end lzw


entity kompresor is
  Port ( 
        digital_in: in STD_LOGIC_VECTOR (11 downto 0);
        digital_out: out STD_LOGIC_VECTOR (12 downto 0)
  );
end kompresor;

architecture Behavioral of kompresor is

    function comp_8b(
     digital_in: STD_LOGIC_VECTOR (11 downto 0))
     return STD_LOGIC_VECTOR is
     variable Selector: std_logic_vector(7 downto 0);
    begin
        
        if (digital_in >= "000000000000") and (digital_in < "000000010000") then
            Selector := "00000000";        
             elsif digital_in >= "000000010000" and digital_in < "000000100000"then Selector := "00000001";
            elsif digital_in >= "000000100000" and digital_in < "000000110000"then Selector := "00000010";
            elsif digital_in >= "000000110000" and digital_in < "000001000000"then Selector := "00000011";
            elsif digital_in >= "000001000000" and digital_in < "000001010000"then Selector := "00000100";
            elsif digital_in >= "000001010000" and digital_in < "000001100000"then Selector := "00000101";
            elsif digital_in >= "000001100000" and digital_in < "000001110000"then Selector := "00000110";
            elsif digital_in >= "000001110000" and digital_in < "000010000000"then Selector := "00000111";
            elsif digital_in >= "000010000000" and digital_in < "000010010000"then Selector := "00001000";
            elsif digital_in >= "000010010000" and digital_in < "000010100000"then Selector := "00001001";
            elsif digital_in >= "000010100000" and digital_in < "000010110000"then Selector := "00001010";
            elsif digital_in >= "000010110000" and digital_in < "000011000000"then Selector := "00001011";
            elsif digital_in >= "000011000000" and digital_in < "000011010000"then Selector := "00001100";
            elsif digital_in >= "000011010000" and digital_in < "000011100000"then Selector := "00001101";
            elsif digital_in >= "000011100000" and digital_in < "000011110000"then Selector := "00001110";
            elsif digital_in >= "000011110000" and digital_in < "000100000000"then Selector := "00001111";
            elsif digital_in >= "000100000000" and digital_in < "000100010000"then Selector := "00010000";
            elsif digital_in >= "000100010000" and digital_in < "000100100000"then Selector := "00010001";
            elsif digital_in >= "000100100000" and digital_in < "000100110000"then Selector := "00010010";
            elsif digital_in >= "000100110000" and digital_in < "000101000000"then Selector := "00010011";
            elsif digital_in >= "000101000000" and digital_in < "000101010000"then Selector := "00010100";
            elsif digital_in >= "000101010000" and digital_in < "000101100000"then Selector := "00010101";
            elsif digital_in >= "000101100000" and digital_in < "000101110000"then Selector := "00010110";
            elsif digital_in >= "000101110000" and digital_in < "000110000000"then Selector := "00010111";
            elsif digital_in >= "000110000000" and digital_in < "000110010000"then Selector := "00011000";
            elsif digital_in >= "000110010000" and digital_in < "000110100000"then Selector := "00011001";
            elsif digital_in >= "000110100000" and digital_in < "000110110000"then Selector := "00011010";
            elsif digital_in >= "000110110000" and digital_in < "000111000000"then Selector := "00011011";
            elsif digital_in >= "000111000000" and digital_in < "000111010000"then Selector := "00011100";
            elsif digital_in >= "000111010000" and digital_in < "000111100000"then Selector := "00011101";
            elsif digital_in >= "000111100000" and digital_in < "000111110000"then Selector := "00011110";
            elsif digital_in >= "000111110000" and digital_in < "001000000000"then Selector := "00011111";
            elsif digital_in >= "001000000000" and digital_in < "001000010000"then Selector := "00100000";
            elsif digital_in >= "001000010000" and digital_in < "001000100000"then Selector := "00100001";
            elsif digital_in >= "001000100000" and digital_in < "001000110000"then Selector := "00100010";
            elsif digital_in >= "001000110000" and digital_in < "001001000000"then Selector := "00100011";
            elsif digital_in >= "001001000000" and digital_in < "001001010000"then Selector := "00100100";
            elsif digital_in >= "001001010000" and digital_in < "001001100000"then Selector := "00100101";
            elsif digital_in >= "001001100000" and digital_in < "001001110000"then Selector := "00100110";
            elsif digital_in >= "001001110000" and digital_in < "001010000000"then Selector := "00100111";
            elsif digital_in >= "001010000000" and digital_in < "001010010000"then Selector := "00101000";
            elsif digital_in >= "001010010000" and digital_in < "001010100000"then Selector := "00101001";
            elsif digital_in >= "001010100000" and digital_in < "001010110000"then Selector := "00101010";
            elsif digital_in >= "001010110000" and digital_in < "001011000000"then Selector := "00101011";
            elsif digital_in >= "001011000000" and digital_in < "001011010000"then Selector := "00101100";
            elsif digital_in >= "001011010000" and digital_in < "001011100000"then Selector := "00101101";
            elsif digital_in >= "001011100000" and digital_in < "001011110000"then Selector := "00101110";
            elsif digital_in >= "001011110000" and digital_in < "001100000000"then Selector := "00101111";
            elsif digital_in >= "001100000000" and digital_in < "001100010000"then Selector := "00110000";
            elsif digital_in >= "001100010000" and digital_in < "001100100000"then Selector := "00110001";
            elsif digital_in >= "001100100000" and digital_in < "001100110000"then Selector := "00110010";
            elsif digital_in >= "001100110000" and digital_in < "001101000000"then Selector := "00110011";
            elsif digital_in >= "001101000000" and digital_in < "001101010000"then Selector := "00110100";
            elsif digital_in >= "001101010000" and digital_in < "001101100000"then Selector := "00110101";
            elsif digital_in >= "001101100000" and digital_in < "001101110000"then Selector := "00110110";
            elsif digital_in >= "001101110000" and digital_in < "001110000000"then Selector := "00110111";
            elsif digital_in >= "001110000000" and digital_in < "001110010000"then Selector := "00111000";
            elsif digital_in >= "001110010000" and digital_in < "001110100000"then Selector := "00111001";
            elsif digital_in >= "001110100000" and digital_in < "001110110000"then Selector := "00111010";
            elsif digital_in >= "001110110000" and digital_in < "001111000000"then Selector := "00111011";
            elsif digital_in >= "001111000000" and digital_in < "001111010000"then Selector := "00111100";
            elsif digital_in >= "001111010000" and digital_in < "001111100000"then Selector := "00111101";
            elsif digital_in >= "001111100000" and digital_in < "001111110000"then Selector := "00111110";
            elsif digital_in >= "001111110000" and digital_in < "010000000000"then Selector := "00111111";
            elsif digital_in >= "010000000000" and digital_in < "010000010000"then Selector := "01000000";
            elsif digital_in >= "010000010000" and digital_in < "010000100000"then Selector := "01000001";
            elsif digital_in >= "010000100000" and digital_in < "010000110000"then Selector := "01000010";
            elsif digital_in >= "010000110000" and digital_in < "010001000000"then Selector := "01000011";
            elsif digital_in >= "010001000000" and digital_in < "010001010000"then Selector := "01000100";
            elsif digital_in >= "010001010000" and digital_in < "010001100000"then Selector := "01000101";
            elsif digital_in >= "010001100000" and digital_in < "010001110000"then Selector := "01000110";
            elsif digital_in >= "010001110000" and digital_in < "010010000000"then Selector := "01000111";
            elsif digital_in >= "010010000000" and digital_in < "010010010000"then Selector := "01001000";
            elsif digital_in >= "010010010000" and digital_in < "010010100000"then Selector := "01001001";
            elsif digital_in >= "010010100000" and digital_in < "010010110000"then Selector := "01001010";
            elsif digital_in >= "010010110000" and digital_in < "010011000000"then Selector := "01001011";
            elsif digital_in >= "010011000000" and digital_in < "010011010000"then Selector := "01001100";
            elsif digital_in >= "010011010000" and digital_in < "010011100000"then Selector := "01001101";
            elsif digital_in >= "010011100000" and digital_in < "010011110000"then Selector := "01001110";
            elsif digital_in >= "010011110000" and digital_in < "010100000000"then Selector := "01001111";
            elsif digital_in >= "010100000000" and digital_in < "010100010000"then Selector := "01010000";
            elsif digital_in >= "010100010000" and digital_in < "010100100000"then Selector := "01010001";
            elsif digital_in >= "010100100000" and digital_in < "010100110000"then Selector := "01010010";
            elsif digital_in >= "010100110000" and digital_in < "010101000000"then Selector := "01010011";
            elsif digital_in >= "010101000000" and digital_in < "010101010000"then Selector := "01010100";
            elsif digital_in >= "010101010000" and digital_in < "010101100000"then Selector := "01010101";
            elsif digital_in >= "010101100000" and digital_in < "010101110000"then Selector := "01010110";
            elsif digital_in >= "010101110000" and digital_in < "010110000000"then Selector := "01010111";
            elsif digital_in >= "010110000000" and digital_in < "010110010000"then Selector := "01011000";
            elsif digital_in >= "010110010000" and digital_in < "010110100000"then Selector := "01011001";
            elsif digital_in >= "010110100000" and digital_in < "010110110000"then Selector := "01011010";
            elsif digital_in >= "010110110000" and digital_in < "010111000000"then Selector := "01011011";
            elsif digital_in >= "010111000000" and digital_in < "010111010000"then Selector := "01011100";
            elsif digital_in >= "010111010000" and digital_in < "010111100000"then Selector := "01011101";
            elsif digital_in >= "010111100000" and digital_in < "010111110000"then Selector := "01011110";
            elsif digital_in >= "010111110000" and digital_in < "011000000000"then Selector := "01011111";
            elsif digital_in >= "011000000000" and digital_in < "011000010000"then Selector := "01100000";
            elsif digital_in >= "011000010000" and digital_in < "011000100000"then Selector := "01100001";
            elsif digital_in >= "011000100000" and digital_in < "011000110000"then Selector := "01100010";
            elsif digital_in >= "011000110000" and digital_in < "011001000000"then Selector := "01100011";
            elsif digital_in >= "011001000000" and digital_in < "011001010000"then Selector := "01100100";
            elsif digital_in >= "011001010000" and digital_in < "011001100000"then Selector := "01100101";
            elsif digital_in >= "011001100000" and digital_in < "011001110000"then Selector := "01100110";
            elsif digital_in >= "011001110000" and digital_in < "011010000000"then Selector := "01100111";
            elsif digital_in >= "011010000000" and digital_in < "011010010000"then Selector := "01101000";
            elsif digital_in >= "011010010000" and digital_in < "011010100000"then Selector := "01101001";
            elsif digital_in >= "011010100000" and digital_in < "011010110000"then Selector := "01101010";
            elsif digital_in >= "011010110000" and digital_in < "011011000000"then Selector := "01101011";
            elsif digital_in >= "011011000000" and digital_in < "011011010000"then Selector := "01101100";
            elsif digital_in >= "011011010000" and digital_in < "011011100000"then Selector := "01101101";
            elsif digital_in >= "011011100000" and digital_in < "011011110000"then Selector := "01101110";
            elsif digital_in >= "011011110000" and digital_in < "011100000000"then Selector := "01101111";
            elsif digital_in >= "011100000000" and digital_in < "011100010000"then Selector := "01110000";
            elsif digital_in >= "011100010000" and digital_in < "011100100000"then Selector := "01110001";
            elsif digital_in >= "011100100000" and digital_in < "011100110000"then Selector := "01110010";
            elsif digital_in >= "011100110000" and digital_in < "011101000000"then Selector := "01110011";
            elsif digital_in >= "011101000000" and digital_in < "011101010000"then Selector := "01110100";
            elsif digital_in >= "011101010000" and digital_in < "011101100000"then Selector := "01110101";
            elsif digital_in >= "011101100000" and digital_in < "011101110000"then Selector := "01110110";
            elsif digital_in >= "011101110000" and digital_in < "011110000000"then Selector := "01110111";
            elsif digital_in >= "011110000000" and digital_in < "011110010000"then Selector := "01111000";
            elsif digital_in >= "011110010000" and digital_in < "011110100000"then Selector := "01111001";
            elsif digital_in >= "011110100000" and digital_in < "011110110000"then Selector := "01111010";
            elsif digital_in >= "011110110000" and digital_in < "011111000000"then Selector := "01111011";
            elsif digital_in >= "011111000000" and digital_in < "011111010000"then Selector := "01111100";
            elsif digital_in >= "011111010000" and digital_in < "011111100000"then Selector := "01111101";
            elsif digital_in >= "011111100000" and digital_in < "011111110000"then Selector := "01111110";
            elsif digital_in >= "011111110000" and digital_in < "100000000000"then Selector := "01111111";
            elsif digital_in >= "100000000000" and digital_in < "100000010000"then Selector := "10000000";
            elsif digital_in >= "100000010000" and digital_in < "100000100000"then Selector := "10000001";
            elsif digital_in >= "100000100000" and digital_in < "100000110000"then Selector := "10000010";
            elsif digital_in >= "100000110000" and digital_in < "100001000000"then Selector := "10000011";
            elsif digital_in >= "100001000000" and digital_in < "100001010000"then Selector := "10000100";
            elsif digital_in >= "100001010000" and digital_in < "100001100000"then Selector := "10000101";
            elsif digital_in >= "100001100000" and digital_in < "100001110000"then Selector := "10000110";
            elsif digital_in >= "100001110000" and digital_in < "100010000000"then Selector := "10000111";
            elsif digital_in >= "100010000000" and digital_in < "100010010000"then Selector := "10001000";
            elsif digital_in >= "100010010000" and digital_in < "100010100000"then Selector := "10001001";
            elsif digital_in >= "100010100000" and digital_in < "100010110000"then Selector := "10001010";
            elsif digital_in >= "100010110000" and digital_in < "100011000000"then Selector := "10001011";
            elsif digital_in >= "100011000000" and digital_in < "100011010000"then Selector := "10001100";
            elsif digital_in >= "100011010000" and digital_in < "100011100000"then Selector := "10001101";
            elsif digital_in >= "100011100000" and digital_in < "100011110000"then Selector := "10001110";
            elsif digital_in >= "100011110000" and digital_in < "100100000000"then Selector := "10001111";
            elsif digital_in >= "100100000000" and digital_in < "100100010000"then Selector := "10010000";
            elsif digital_in >= "100100010000" and digital_in < "100100100000"then Selector := "10010001";
            elsif digital_in >= "100100100000" and digital_in < "100100110000"then Selector := "10010010";
            elsif digital_in >= "100100110000" and digital_in < "100101000000"then Selector := "10010011";
            elsif digital_in >= "100101000000" and digital_in < "100101010000"then Selector := "10010100";
            elsif digital_in >= "100101010000" and digital_in < "100101100000"then Selector := "10010101";
            elsif digital_in >= "100101100000" and digital_in < "100101110000"then Selector := "10010110";
            elsif digital_in >= "100101110000" and digital_in < "100110000000"then Selector := "10010111";
            elsif digital_in >= "100110000000" and digital_in < "100110010000"then Selector := "10011000";
            elsif digital_in >= "100110010000" and digital_in < "100110100000"then Selector := "10011001";
            elsif digital_in >= "100110100000" and digital_in < "100110110000"then Selector := "10011010";
            elsif digital_in >= "100110110000" and digital_in < "100111000000"then Selector := "10011011";
            elsif digital_in >= "100111000000" and digital_in < "100111010000"then Selector := "10011100";
            elsif digital_in >= "100111010000" and digital_in < "100111100000"then Selector := "10011101";
            elsif digital_in >= "100111100000" and digital_in < "100111110000"then Selector := "10011110";
            elsif digital_in >= "100111110000" and digital_in < "101000000000"then Selector := "10011111";
            elsif digital_in >= "101000000000" and digital_in < "101000010000"then Selector := "10100000";
            elsif digital_in >= "101000010000" and digital_in < "101000100000"then Selector := "10100001";
            elsif digital_in >= "101000100000" and digital_in < "101000110000"then Selector := "10100010";
            elsif digital_in >= "101000110000" and digital_in < "101001000000"then Selector := "10100011";
            elsif digital_in >= "101001000000" and digital_in < "101001010000"then Selector := "10100100";
            elsif digital_in >= "101001010000" and digital_in < "101001100000"then Selector := "10100101";
            elsif digital_in >= "101001100000" and digital_in < "101001110000"then Selector := "10100110";
            elsif digital_in >= "101001110000" and digital_in < "101010000000"then Selector := "10100111";
            elsif digital_in >= "101010000000" and digital_in < "101010010000"then Selector := "10101000";
            elsif digital_in >= "101010010000" and digital_in < "101010100000"then Selector := "10101001";
            elsif digital_in >= "101010100000" and digital_in < "101010110000"then Selector := "10101010";
            elsif digital_in >= "101010110000" and digital_in < "101011000000"then Selector := "10101011";
            elsif digital_in >= "101011000000" and digital_in < "101011010000"then Selector := "10101100";
            elsif digital_in >= "101011010000" and digital_in < "101011100000"then Selector := "10101101";
            elsif digital_in >= "101011100000" and digital_in < "101011110000"then Selector := "10101110";
            elsif digital_in >= "101011110000" and digital_in < "101100000000"then Selector := "10101111";
            elsif digital_in >= "101100000000" and digital_in < "101100010000"then Selector := "10110000";
            elsif digital_in >= "101100010000" and digital_in < "101100100000"then Selector := "10110001";
            elsif digital_in >= "101100100000" and digital_in < "101100110000"then Selector := "10110010";
            elsif digital_in >= "101100110000" and digital_in < "101101000000"then Selector := "10110011";
            elsif digital_in >= "101101000000" and digital_in < "101101010000"then Selector := "10110100";
            elsif digital_in >= "101101010000" and digital_in < "101101100000"then Selector := "10110101";
            elsif digital_in >= "101101100000" and digital_in < "101101110000"then Selector := "10110110";
            elsif digital_in >= "101101110000" and digital_in < "101110000000"then Selector := "10110111";
            elsif digital_in >= "101110000000" and digital_in < "101110010000"then Selector := "10111000";
            elsif digital_in >= "101110010000" and digital_in < "101110100000"then Selector := "10111001";
            elsif digital_in >= "101110100000" and digital_in < "101110110000"then Selector := "10111010";
            elsif digital_in >= "101110110000" and digital_in < "101111000000"then Selector := "10111011";
            elsif digital_in >= "101111000000" and digital_in < "101111010000"then Selector := "10111100";
            elsif digital_in >= "101111010000" and digital_in < "101111100000"then Selector := "10111101";
            elsif digital_in >= "101111100000" and digital_in < "101111110000"then Selector := "10111110";
            elsif digital_in >= "101111110000" and digital_in < "110000000000"then Selector := "10111111";
            elsif digital_in >= "110000000000" and digital_in < "110000010000"then Selector := "11000000";
            elsif digital_in >= "110000010000" and digital_in < "110000100000"then Selector := "11000001";
            elsif digital_in >= "110000100000" and digital_in < "110000110000"then Selector := "11000010";
            elsif digital_in >= "110000110000" and digital_in < "110001000000"then Selector := "11000011";
            elsif digital_in >= "110001000000" and digital_in < "110001010000"then Selector := "11000100";
            elsif digital_in >= "110001010000" and digital_in < "110001100000"then Selector := "11000101";
            elsif digital_in >= "110001100000" and digital_in < "110001110000"then Selector := "11000110";
            elsif digital_in >= "110001110000" and digital_in < "110010000000"then Selector := "11000111";
            elsif digital_in >= "110010000000" and digital_in < "110010010000"then Selector := "11001000";
            elsif digital_in >= "110010010000" and digital_in < "110010100000"then Selector := "11001001";
            elsif digital_in >= "110010100000" and digital_in < "110010110000"then Selector := "11001010";
            elsif digital_in >= "110010110000" and digital_in < "110011000000"then Selector := "11001011";
            elsif digital_in >= "110011000000" and digital_in < "110011010000"then Selector := "11001100";
            elsif digital_in >= "110011010000" and digital_in < "110011100000"then Selector := "11001101";
            elsif digital_in >= "110011100000" and digital_in < "110011110000"then Selector := "11001110";
            elsif digital_in >= "110011110000" and digital_in < "110100000000"then Selector := "11001111";
            elsif digital_in >= "110100000000" and digital_in < "110100010000"then Selector := "11010000";
            elsif digital_in >= "110100010000" and digital_in < "110100100000"then Selector := "11010001";
            elsif digital_in >= "110100100000" and digital_in < "110100110000"then Selector := "11010010";
            elsif digital_in >= "110100110000" and digital_in < "110101000000"then Selector := "11010011";
            elsif digital_in >= "110101000000" and digital_in < "110101010000"then Selector := "11010100";
            elsif digital_in >= "110101010000" and digital_in < "110101100000"then Selector := "11010101";
            elsif digital_in >= "110101100000" and digital_in < "110101110000"then Selector := "11010110";
            elsif digital_in >= "110101110000" and digital_in < "110110000000"then Selector := "11010111";
            elsif digital_in >= "110110000000" and digital_in < "110110010000"then Selector := "11011000";
            elsif digital_in >= "110110010000" and digital_in < "110110100000"then Selector := "11011001";
            elsif digital_in >= "110110100000" and digital_in < "110110110000"then Selector := "11011010";
            elsif digital_in >= "110110110000" and digital_in < "110111000000"then Selector := "11011011";
            elsif digital_in >= "110111000000" and digital_in < "110111010000"then Selector := "11011100";
            elsif digital_in >= "110111010000" and digital_in < "110111100000"then Selector := "11011101";
            elsif digital_in >= "110111100000" and digital_in < "110111110000"then Selector := "11011110";
            elsif digital_in >= "110111110000" and digital_in < "111000000000"then Selector := "11011111";
            elsif digital_in >= "111000000000" and digital_in < "111000010000"then Selector := "11100000";
            elsif digital_in >= "111000010000" and digital_in < "111000100000"then Selector := "11100001";
            elsif digital_in >= "111000100000" and digital_in < "111000110000"then Selector := "11100010";
            elsif digital_in >= "111000110000" and digital_in < "111001000000"then Selector := "11100011";
            elsif digital_in >= "111001000000" and digital_in < "111001010000"then Selector := "11100100";
            elsif digital_in >= "111001010000" and digital_in < "111001100000"then Selector := "11100101";
            elsif digital_in >= "111001100000" and digital_in < "111001110000"then Selector := "11100110";
            elsif digital_in >= "111001110000" and digital_in < "111010000000"then Selector := "11100111";
            elsif digital_in >= "111010000000" and digital_in < "111010010000"then Selector := "11101000";
            elsif digital_in >= "111010010000" and digital_in < "111010100000"then Selector := "11101001";
            elsif digital_in >= "111010100000" and digital_in < "111010110000"then Selector := "11101010";
            elsif digital_in >= "111010110000" and digital_in < "111011000000"then Selector := "11101011";
            elsif digital_in >= "111011000000" and digital_in < "111011010000"then Selector := "11101100";
            elsif digital_in >= "111011010000" and digital_in < "111011100000"then Selector := "11101101";
            elsif digital_in >= "111011100000" and digital_in < "111011110000"then Selector := "11101110";
            elsif digital_in >= "111011110000" and digital_in < "111100000000"then Selector := "11101111";
            elsif digital_in >= "111100000000" and digital_in < "111100010000"then Selector := "11110000";
            elsif digital_in >= "111100010000" and digital_in < "111100100000"then Selector := "11110001";
            elsif digital_in >= "111100100000" and digital_in < "111100110000"then Selector := "11110010";
            elsif digital_in >= "111100110000" and digital_in < "111101000000"then Selector := "11110011";
            elsif digital_in >= "111101000000" and digital_in < "111101010000"then Selector := "11110100";
            elsif digital_in >= "111101010000" and digital_in < "111101100000"then Selector := "11110101";
            elsif digital_in >= "111101100000" and digital_in < "111101110000"then Selector := "11110110";
            elsif digital_in >= "111101110000" and digital_in < "111110000000"then Selector := "11110111";
            elsif digital_in >= "111110000000" and digital_in < "111110010000"then Selector := "11111000";
            elsif digital_in >= "111110010000" and digital_in < "111110100000"then Selector := "11111001";
            elsif digital_in >= "111110100000" and digital_in < "111110110000"then Selector := "11111010";
            elsif digital_in >= "111110110000" and digital_in < "111111000000"then Selector := "11111011";
            elsif digital_in >= "111111000000" and digital_in < "111111010000"then Selector := "11111100";
            elsif digital_in >= "111111010000" and digital_in < "111111100000"then Selector := "11111101";
            elsif digital_in >= "111111100000" and digital_in < "111111110000"then Selector := "11111110";            
            elsif digital_in >= "111111110000" and digital_in < "000000000000"then Selector := "11111111";


          else
          Selector:= "00000000";
         end if;    
       -- if digital_in < "00000000" and digital_in >= "11111111" then
                -- Selector := "00";               
        
            
          return Selector;
      end comp_8b;
      
      function decomp_8b(
         Selector: STD_LOGIC_VECTOR (7 downto 0))
         return STD_LOGIC_VECTOR is
         variable digital_out: std_logic_vector(12 downto 0);
        begin
             
            case Selector is
            when "00000000" => digital_out := "0000000000000";
            when "00000001" => digital_out := "0000000100000";
            when "00000010" => digital_out := "0000001000000";
            when "00000011" => digital_out := "0000001100000";
            when "00000100" => digital_out := "0000010000000";
            when "00000101" => digital_out := "0000010100000";
            when "00000110" => digital_out := "0000011000000";
            when "00000111" => digital_out := "0000011100000";
            when "00001000" => digital_out := "0000100000000";
            when "00001001" => digital_out := "0000100100000";
            when "00001010" => digital_out := "0000101000000";
            when "00001011" => digital_out := "0000101100000";
            when "00001100" => digital_out := "0000110000000";
            when "00001101" => digital_out := "0000110100000";
            when "00001110" => digital_out := "0000111000000";
            when "00001111" => digital_out := "0000111100000";
            when "00010000" => digital_out := "0001000000000";
            when "00010001" => digital_out := "0001000100000";
            when "00010010" => digital_out := "0001001000000";
            when "00010011" => digital_out := "0001001100000";
            when "00010100" => digital_out := "0001010000000";
            when "00010101" => digital_out := "0001010100000";
            when "00010110" => digital_out := "0001011000000";
            when "00010111" => digital_out := "0001011100000";
            when "00011000" => digital_out := "0001100000000";
            when "00011001" => digital_out := "0001100100000";
            when "00011010" => digital_out := "0001101000000";
            when "00011011" => digital_out := "0001101100000";
            when "00011100" => digital_out := "0001110000000";
            when "00011101" => digital_out := "0001110100000";
            when "00011110" => digital_out := "0001111000000";
            when "00011111" => digital_out := "0001111100000";
            when "00100000" => digital_out := "0010000000000";
            when "00100001" => digital_out := "0010000100000";
            when "00100010" => digital_out := "0010001000000";
            when "00100011" => digital_out := "0010001100000";
            when "00100100" => digital_out := "0010010000000";
            when "00100101" => digital_out := "0010010100000";
            when "00100110" => digital_out := "0010011000000";
            when "00100111" => digital_out := "0010011100000";
            when "00101000" => digital_out := "0010100000000";
            when "00101001" => digital_out := "0010100100000";
            when "00101010" => digital_out := "0010101000000";
            when "00101011" => digital_out := "0010101100000";
            when "00101100" => digital_out := "0010110000000";
            when "00101101" => digital_out := "0010110100000";
            when "00101110" => digital_out := "0010111000000";
            when "00101111" => digital_out := "0010111100000";
            when "00110000" => digital_out := "0011000000000";
            when "00110001" => digital_out := "0011000100000";
            when "00110010" => digital_out := "0011001000000";
            when "00110011" => digital_out := "0011001100000";
            when "00110100" => digital_out := "0011010000000";
            when "00110101" => digital_out := "0011010100000";
            when "00110110" => digital_out := "0011011000000";
            when "00110111" => digital_out := "0011011100000";
            when "00111000" => digital_out := "0011100000000";
            when "00111001" => digital_out := "0011100100000";
            when "00111010" => digital_out := "0011101000000";
            when "00111011" => digital_out := "0011101100000";
            when "00111100" => digital_out := "0011110000000";
            when "00111101" => digital_out := "0011110100000";
            when "00111110" => digital_out := "0011111000000";
            when "00111111" => digital_out := "0011111100000";
            when "01000000" => digital_out := "0100000000000";
            when "01000001" => digital_out := "0100000100000";
            when "01000010" => digital_out := "0100001000000";
            when "01000011" => digital_out := "0100001100000";
            when "01000100" => digital_out := "0100010000000";
            when "01000101" => digital_out := "0100010100000";
            when "01000110" => digital_out := "0100011000000";
            when "01000111" => digital_out := "0100011100000";
            when "01001000" => digital_out := "0100100000000";
            when "01001001" => digital_out := "0100100100000";
            when "01001010" => digital_out := "0100101000000";
            when "01001011" => digital_out := "0100101100000";
            when "01001100" => digital_out := "0100110000000";
            when "01001101" => digital_out := "0100110100000";
            when "01001110" => digital_out := "0100111000000";
            when "01001111" => digital_out := "0100111100000";
            when "01010000" => digital_out := "0101000000000";
            when "01010001" => digital_out := "0101000100000";
            when "01010010" => digital_out := "0101001000000";
            when "01010011" => digital_out := "0101001100000";
            when "01010100" => digital_out := "0101010000000";
            when "01010101" => digital_out := "0101010100000";
            when "01010110" => digital_out := "0101011000000";
            when "01010111" => digital_out := "0101011100000";
            when "01011000" => digital_out := "0101100000000";
            when "01011001" => digital_out := "0101100100000";
            when "01011010" => digital_out := "0101101000000";
            when "01011011" => digital_out := "0101101100000";
            when "01011100" => digital_out := "0101110000000";
            when "01011101" => digital_out := "0101110100000";
            when "01011110" => digital_out := "0101111000000";
            when "01011111" => digital_out := "0101111100000";
            when "01100000" => digital_out := "0110000000000";
            when "01100001" => digital_out := "0110000100000";
            when "01100010" => digital_out := "0110001000000";
            when "01100011" => digital_out := "0110001100000";
            when "01100100" => digital_out := "0110010000000";
            when "01100101" => digital_out := "0110010100000";
            when "01100110" => digital_out := "0110011000000";
            when "01100111" => digital_out := "0110011100000";
            when "01101000" => digital_out := "0110100000000";
            when "01101001" => digital_out := "0110100100000";
            when "01101010" => digital_out := "0110101000000";
            when "01101011" => digital_out := "0110101100000";
            when "01101100" => digital_out := "0110110000000";
            when "01101101" => digital_out := "0110110100000";
            when "01101110" => digital_out := "0110111000000";
            when "01101111" => digital_out := "0110111100000";
            when "01110000" => digital_out := "0111000000000";
            when "01110001" => digital_out := "0111000100000";
            when "01110010" => digital_out := "0111001000000";
            when "01110011" => digital_out := "0111001100000";
            when "01110100" => digital_out := "0111010000000";
            when "01110101" => digital_out := "0111010100000";
            when "01110110" => digital_out := "0111011000000";
            when "01110111" => digital_out := "0111011100000";
            when "01111000" => digital_out := "0111100000000";
            when "01111001" => digital_out := "0111100100000";
            when "01111010" => digital_out := "0111101000000";
            when "01111011" => digital_out := "0111101100000";
            when "01111100" => digital_out := "0111110000000";
            when "01111101" => digital_out := "0111110100000";
            when "01111110" => digital_out := "0111111000000";
            when "01111111" => digital_out := "0111111100000";
            when "10000000" => digital_out := "1000000000000";
            when "10000001" => digital_out := "1000000100000";
            when "10000010" => digital_out := "1000001000000";
            when "10000011" => digital_out := "1000001100000";
            when "10000100" => digital_out := "1000010000000";
            when "10000101" => digital_out := "1000010100000";
            when "10000110" => digital_out := "1000011000000";
            when "10000111" => digital_out := "1000011100000";
            when "10001000" => digital_out := "1000100000000";
            when "10001001" => digital_out := "1000100100000";
            when "10001010" => digital_out := "1000101000000";
            when "10001011" => digital_out := "1000101100000";
            when "10001100" => digital_out := "1000110000000";
            when "10001101" => digital_out := "1000110100000";
            when "10001110" => digital_out := "1000111000000";
            when "10001111" => digital_out := "1000111100000";
            when "10010000" => digital_out := "1001000000000";
            when "10010001" => digital_out := "1001000100000";
            when "10010010" => digital_out := "1001001000000";
            when "10010011" => digital_out := "1001001100000";
            when "10010100" => digital_out := "1001010000000";
            when "10010101" => digital_out := "1001010100000";
            when "10010110" => digital_out := "1001011000000";
            when "10010111" => digital_out := "1001011100000";
            when "10011000" => digital_out := "1001100000000";
            when "10011001" => digital_out := "1001100100000";
            when "10011010" => digital_out := "1001101000000";
            when "10011011" => digital_out := "1001101100000";
            when "10011100" => digital_out := "1001110000000";
            when "10011101" => digital_out := "1001110100000";
            when "10011110" => digital_out := "1001111000000";
            when "10011111" => digital_out := "1001111100000";
            when "10100000" => digital_out := "1010000000000";
            when "10100001" => digital_out := "1010000100000";
            when "10100010" => digital_out := "1010001000000";
            when "10100011" => digital_out := "1010001100000";
            when "10100100" => digital_out := "1010010000000";
            when "10100101" => digital_out := "1010010100000";
            when "10100110" => digital_out := "1010011000000";
            when "10100111" => digital_out := "1010011100000";
            when "10101000" => digital_out := "1010100000000";
            when "10101001" => digital_out := "1010100100000";
            when "10101010" => digital_out := "1010101000000";
            when "10101011" => digital_out := "1010101100000";
            when "10101100" => digital_out := "1010110000000";
            when "10101101" => digital_out := "1010110100000";
            when "10101110" => digital_out := "1010111000000";
            when "10101111" => digital_out := "1010111100000";
            when "10110000" => digital_out := "1011000000000";
            when "10110001" => digital_out := "1011000100000";
            when "10110010" => digital_out := "1011001000000";
            when "10110011" => digital_out := "1011001100000";
            when "10110100" => digital_out := "1011010000000";
            when "10110101" => digital_out := "1011010100000";
            when "10110110" => digital_out := "1011011000000";
            when "10110111" => digital_out := "1011011100000";
            when "10111000" => digital_out := "1011100000000";
            when "10111001" => digital_out := "1011100100000";
            when "10111010" => digital_out := "1011101000000";
            when "10111011" => digital_out := "1011101100000";
            when "10111100" => digital_out := "1011110000000";
            when "10111101" => digital_out := "1011110100000";
            when "10111110" => digital_out := "1011111000000";
            when "10111111" => digital_out := "1011111100000";
            when "11000000" => digital_out := "1100000000000";
            when "11000001" => digital_out := "1100000100000";
            when "11000010" => digital_out := "1100001000000";
            when "11000011" => digital_out := "1100001100000";
            when "11000100" => digital_out := "1100010000000";
            when "11000101" => digital_out := "1100010100000";
            when "11000110" => digital_out := "1100011000000";
            when "11000111" => digital_out := "1100011100000";
            when "11001000" => digital_out := "1100100000000";
            when "11001001" => digital_out := "1100100100000";
            when "11001010" => digital_out := "1100101000000";
            when "11001011" => digital_out := "1100101100000";
            when "11001100" => digital_out := "1100110000000";
            when "11001101" => digital_out := "1100110100000";
            when "11001110" => digital_out := "1100111000000";
            when "11001111" => digital_out := "1100111100000";
            when "11010000" => digital_out := "1101000000000";
            when "11010001" => digital_out := "1101000100000";
            when "11010010" => digital_out := "1101001000000";
            when "11010011" => digital_out := "1101001100000";
            when "11010100" => digital_out := "1101010000000";
            when "11010101" => digital_out := "1101010100000";
            when "11010110" => digital_out := "1101011000000";
            when "11010111" => digital_out := "1101011100000";
            when "11011000" => digital_out := "1101100000000";
            when "11011001" => digital_out := "1101100100000";
            when "11011010" => digital_out := "1101101000000";
            when "11011011" => digital_out := "1101101100000";
            when "11011100" => digital_out := "1101110000000";
            when "11011101" => digital_out := "1101110100000";
            when "11011110" => digital_out := "1101111000000";
            when "11011111" => digital_out := "1101111100000";
            when "11100000" => digital_out := "1110000000000";
            when "11100001" => digital_out := "1110000100000";
            when "11100010" => digital_out := "1110001000000";
            when "11100011" => digital_out := "1110001100000";
            when "11100100" => digital_out := "1110010000000";
            when "11100101" => digital_out := "1110010100000";
            when "11100110" => digital_out := "1110011000000";
            when "11100111" => digital_out := "1110011100000";
            when "11101000" => digital_out := "1110100000000";
            when "11101001" => digital_out := "1110100100000";
            when "11101010" => digital_out := "1110101000000";
            when "11101011" => digital_out := "1110101100000";
            when "11101100" => digital_out := "1110110000000";
            when "11101101" => digital_out := "1110110100000";
            when "11101110" => digital_out := "1110111000000";
            when "11101111" => digital_out := "1110111100000";
            when "11110000" => digital_out := "1111000000000";
            when "11110001" => digital_out := "1111000100000";
            when "11110010" => digital_out := "1111001000000";
            when "11110011" => digital_out := "1111001100000";
            when "11110100" => digital_out := "1111010000000";
            when "11110101" => digital_out := "1111010100000";
            when "11110110" => digital_out := "1111011000000";
            when "11110111" => digital_out := "1111011100000";
            when "11111000" => digital_out := "1111100000000";
            when "11111001" => digital_out := "1111100100000";
            when "11111010" => digital_out := "1111101000000";
            when "11111011" => digital_out := "1111101100000";
            when "11111100" => digital_out := "1111110000000";
            when "11111101" => digital_out := "1111110100000";
            when "11111110" => digital_out := "1111111000000";
            when "11111111" => digital_out := "1111111100000";
            

            end case;                     
             
              return digital_out;
          end decomp_8b;    

begin

    digital_out <= decomp_8b(comp_8b(digital_in));


end Behavioral;



-- Index: 0  1   2   3
-- Value: 0 64 128 192



