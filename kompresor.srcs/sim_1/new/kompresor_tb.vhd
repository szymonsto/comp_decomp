----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.01.2020 13:12:28
-- Design Name: 
-- Module Name: kompresor_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kompresor_tb is

end kompresor_tb;

architecture Behavioral of kompresor_tb is

    component kompresor
        Port ( 
            digital_in: in STD_LOGIC_VECTOR (7 downto 0);
            digital_out: out STD_LOGIC_VECTOR (7 downto 0)
      );
    end component;

    signal digital_in : std_logic_vector(7 downto 0);
  signal digital_out : std_logic_vector(7 downto 0);

begin

    cde :  kompresor
        port map(digital_in => digital_in,
        digital_out => digital_out);
        

stimulus: process
  begin
    digital_in <= "00001101";
    wait for 5 ns;
    digital_in <= "01001110";
        wait for 5 ns;
    digital_in <= "11001100";
          wait for 5 ns;
     digital_in <= "10001110";
           wait for 5 ns;
     digital_in <= "10010100";
            wait for 5 ns;
     digital_in <= "11001101";
            wait for 5 ns;
     digital_in <= "11111111";
               wait for 5 ns;
         wait;
      end process;


end Behavioral;
